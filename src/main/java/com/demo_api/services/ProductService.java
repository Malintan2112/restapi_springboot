package com.demo_api.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.demo_api.models.entity.Product;
import com.demo_api.models.entity.Supplier;
import com.demo_api.models.repository.ProductRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductService {
    @Autowired
    private ProductRepo productRepo;

    public Product create (Product product) {
        return productRepo.save(product);
    }

    public Product findOne(Long id) {
        Optional <Product> product = productRepo.findById(id);
        if (!product.isPresent()) {
            return null;
        }
        return product.get();
    }

    public Iterable<Product> findAll() {
        return productRepo.findAll();
    }

    public void removeOne (Long id) {
        productRepo.deleteById(id);
    }

    public List<Product> findByName (String name){
        return productRepo.findByNameContains(name);
    }

    public void addSuppliers (Supplier supplier, Long productId){
        Product product = findOne(productId);
        if (product == null) {
            throw new RuntimeException("Product wiht ID : "+productId+" not found");
        }
        product.getSuppliers().add(supplier);
        create(product);
    }
}
