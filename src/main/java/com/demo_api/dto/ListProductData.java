package com.demo_api.dto;

import java.util.ArrayList;
import java.util.List;

import com.demo_api.models.entity.Category;
import com.demo_api.models.entity.Product;
import com.demo_api.models.entity.Supplier;

public class ListProductData<T> {
    private Long id;
    private String name;
    private List<ListSupplierData> supplierData= new ArrayList<>();
    private Category category;
    public ListProductData() {

    }
    public ListProductData(Product product) {
        this.setId(product.getId());
        this.setName(product.getName());
        this.setCategory(product.getCategory());
        for(Supplier supplier : product.getSuppliers()){
            ListSupplierData listSupplierData = new ListSupplierData();
            listSupplierData.setId(supplier.getId());
            listSupplierData.setName(supplier.getName());
            supplierData.add(listSupplierData);
        }
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   

    public List<ListSupplierData> getSupplierData() {
        return supplierData;
    }

    public void setSupplierData(List<ListSupplierData> supplierData) {
        this.supplierData = supplierData;
    }

    
}
