package com.demo_api.dto;

import java.util.ArrayList;
import java.util.List;

public class ListSupplierData {
    private Long id;
    private String name;
    private List<ListProductData> productData =  new ArrayList<>();
    public ListSupplierData() {
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
}
