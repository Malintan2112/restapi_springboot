package com.demo_api.dto;

import java.util.ArrayList;
import java.util.List;

public class ResponseData <T>{
    private boolean status;
    private List<String> messages = new ArrayList<>();
    private T payload;
    private ArrayList data;
    public boolean isStatus() {
        return status;
    }
    public ArrayList getData() {
        return data;
    }
    public void setData(ArrayList data) {
        this.data = data;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public List<String> getMessages() {
        return messages;
    }
    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
    public T getPayload() {
        return payload;
    }
    public void setPayload(T payload) {
        this.payload = payload;
    }
}
