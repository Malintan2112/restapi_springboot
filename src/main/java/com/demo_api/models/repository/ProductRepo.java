package com.demo_api.models.repository;

import java.util.List;

import com.demo_api.models.entity.Product;

import org.springframework.data.repository.CrudRepository;

public interface ProductRepo extends CrudRepository<Product,Long> {
    List<Product> findByNameContains(String name);
}
